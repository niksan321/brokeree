﻿using Brokeree.DataAccess;
using Microsoft.Owin.StaticFiles;
using Owin;
using System.IO;
using System.Web.Http;

namespace Brokeree
{
    public class Startup
    {
        /// <summary>
        /// Настройка hhtp сервера
        /// </summary>
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            UseStaticCsvFiles(app);
            UseStaticSdfFiles(app);
            app.UseWebApi(config);
        }

        /// <summary>
        /// Настройка статических файлов csv
        /// </summary>
        void UseStaticCsvFiles(IAppBuilder app)
        {
            Directory.CreateDirectory(CsvFile.GetBaseFolder());
            app.UseStaticFiles($"/{CsvFile.FOLDER}");
        }

        /// <summary>
        /// Настройка статических файлов csv
        /// </summary>
        void UseStaticSdfFiles(IAppBuilder app)
        {
            Directory.CreateDirectory(SdfFile.GetBaseFolder());
            var sdfOptions = new StaticFileOptions
            {
                ServeUnknownFileTypes = true,
                DefaultContentType = "application/octet-stream",
                RequestPath = new Microsoft.Owin.PathString($"/{SdfFile.FOLDER}")
            };
            app.UseStaticFiles(sdfOptions);
        }
    }
}
