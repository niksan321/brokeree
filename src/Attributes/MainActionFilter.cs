﻿using NLog;
using System.Web.Http.Filters;
using System;

namespace Brokeree.Attributes
{
    /// <summary>
    /// Логирование всех запросов и ответов к api, а также ошибок
    /// </summary>
    class ActionLogAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Лог
        /// </summary>
        static Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Логирование выполнения всех запросов, а также возвращаемых ошибок
        /// </summary>
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            var message = $"Обработано: {Environment.NewLine}";
            message += GetRequestMsg(context);
            message += GetResponceMsg(context);
            message += GetErrorMsg(context);
            log.Trace(message);
        }

        /// <summary>
        /// Возвращает описание ошибки запроса
        /// </summary>
        static string GetErrorMsg(HttpActionExecutedContext context)
        {
            var exception = context.Exception;
            if (exception != null)
            {
                return $"Ответ: Ошибка: {exception.Message}";
            }
            return null;
        }

        /// <summary>
        /// Возвращает описание ответа
        /// </summary>
        static string GetResponceMsg(HttpActionExecutedContext context)
        {
            var responce = context.Response;
            if (responce != null)
            {
                var content = responce?.Content.ReadAsStringAsync().Result;
                return $"Ответ: Статус: {responce.StatusCode} Контент: {content}";
            }
            return null;
        }

        /// <summary>
        /// Возвращает описание запроса
        /// </summary>
        static string GetRequestMsg(HttpActionExecutedContext context)
        {
            var request = context.Request;
            if (request != null)
            {
                var content = request.Content?.ReadAsStringAsync().Result;
                var requestMsg = $"Запрос: URL: {request.RequestUri} Метод: {request.Method} Контент: {content}";
                return $"{requestMsg}{Environment.NewLine}";
            }
            return null;
        }
    }
}
