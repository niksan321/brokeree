﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Brokeree.Attributes
{
    /// <summary>
    /// Фильтр ошибок
    /// </summary>
    class MainExceptionFilterAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// Возвращает пользователю только текст ошибки без лишних параметров и стэктрейса
        /// </summary>
        public override void OnException(HttpActionExecutedContext context)
        {
            var error = context.Exception.Message;
            context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, error);
        }
    }
}
