﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brokeree.Services
{
    public interface IConvertProvider<T>
    {
        void AddConverter(T source, IConverter<T> converter);
        IConverter<T> GetConverter(T source);
    }

    public interface IConverter<T>
    {
        void Start();
        void Stop();
        int GetProgress();
        IList<string> GetLinks();
        IList<string> GetFullNames();
        void Init(T source);
    }

    public interface IGetRecordSupport<E>
    {
        E GetRecord(long id);
    }

    public interface IDeleteByNameSupport
    {
        void DeleteByName(string name);
    }
}
