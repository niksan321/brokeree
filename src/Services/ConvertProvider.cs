﻿using Brokeree.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brokeree.Services
{
    public abstract class ConvertProvider<T> : IConvertProvider<T>
    {
        ConcurrentDictionary<T, IConverter<T>> sources { get; set; }

        public virtual void AddConverter(T source, IConverter<T> converter)
        {
            sources.TryAdd(source, converter);
        }

        public virtual IConverter<T> GetConverter(T source)
        {
            return sources[source];
        }
    }
}
