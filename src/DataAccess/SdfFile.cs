﻿using System;
using System.IO;
using System.Data.SqlServerCe;
using System.Data;

namespace Brokeree.DataAccess
{
    /// <summary>
    /// Работа с sdf файлом базы данных
    /// (работа осуществляется через выполнение прямых sql запросов
    /// при обработки большого количества записей в пользу скорости обработки
    /// я отказался от EntityFramework)
    /// </summary>
    public class SdfFile : IDisposable
    {
        /// <summary>
        /// Имя каталога для сохранения файлов
        /// относительно базового каталога приложения
        /// </summary>
        public const string FOLDER = "sdf";

        /// <summary>
        /// локальный путь к файлу Csv
        /// </summary>
        public string FullFileName { get; }

        /// <summary>
        /// Поток Csv файла
        /// </summary>
        SqlCeConnection connection;

        /// <summary>
        /// Создание/отркытие файла бд
        /// </summary>
        /// <param name="name">имя файла</param>
        /// <param name="isCreateNew">если True, то создаётся новая бд
        /// если False, то открывается существующая</param>
        public SdfFile(string name, bool isCreateNew)
        {
            FullFileName = GetFullNameBySdfFileName(name);
            var connectionString = $"Data Source={FullFileName}";

            if (isCreateNew)
            {
                if (File.Exists(FullFileName))
                {
                    throw new Exception($"Невозможно создать файл {name}, он уже существует. Удалите файл и повторите попытку");
                }
                using (var engine = new SqlCeEngine(connectionString))
                {
                    engine.CreateDatabase();
                }
            }
            else
            {
                if (!File.Exists(FullFileName))
                {
                    throw new Exception($"Файл {name} не существует");
                }
            }

            connection = new SqlCeConnection(connectionString);
            connection.Open();
        }

        /// <summary>
        /// Возвращает обновляемый, прокручиваемый курсор
        /// </summary>
        /// <param name="command">команда для которой необходим курсор</param>
        public SqlCeResultSet GetSet(SqlCeCommand command)
        {
            return command.ExecuteResultSet(ResultSetOptions.Updatable | ResultSetOptions.Scrollable);
        }

        /// <summary>
        /// Возвращает команду для прямого обращения к таблице
        /// (один из самых быстрых методов для работы с большим количеством записей)
        /// </summary>
        /// <param name="tableName">имя таблицы для прямого доступа</param>
        public SqlCeCommand GetDirectCommand(string tableName)
        {
            var command = new SqlCeCommand();
            command.Connection = connection;
            command.CommandText = tableName;
            command.CommandType = CommandType.TableDirect;
            return command;
        }

        /// <summary>
        /// Возвращает команду по переданному sql запросу
        /// </summary>
        /// <param name="query">sql запрос</param>
        public SqlCeCommand GetCommand(string query)
        {
            var command = new SqlCeCommand();
            command.Connection = connection;
            command.CommandText = query;
            return command;
        }

        /// <summary>
        /// выполнение sql запроса (не на выборку)
        /// </summary>
        /// <param name="sql">запрос который необходимо выполнить</param>
        public void ExecuteSqlNoneQuery(string sql)
        {
            using (var command = new SqlCeCommand())
            {
                command.Connection = connection;
                command.CommandText = sql;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Получить полный путь к каталогу csv файлов
        /// </summary>
        public static string GetBaseFolder()
        {
            return AppDomain.CurrentDomain.BaseDirectory + FOLDER;
        }

        /// <summary>
        /// Возвращает имя sdf файла бд на основе имени бинарного файла
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>имя sdf файла бд</returns>
        public static string GetName(string fullFileName)
        {
            return $"{Path.GetFileNameWithoutExtension(fullFileName)}.sdf";
        }

        /// <summary>
        /// Возвращает полный локальный путь к sdf файлу
        /// </summary>
        /// <param name="name">имя sdf файла</param>
        /// <returns>полный локальный путь к sdf файлу</returns>
        public static string GetFullNameBySdfFileName(string name)
        {
            var folder = GetBaseFolder();
            return $"{folder}\\{name}";
        }

        /// <summary>
        /// Возвращает полный локальный путь к sdf файлу
        /// </summary>
        /// <param name="fullFileName">полное локальное имя бинарного файла</param>
        /// <returns>полный локальный путь к sdf файлу</returns>
        public static string GetFullNameByBinaryFileName(string fullFileName)
        {
            var name = GetName(fullFileName);
            return GetFullNameBySdfFileName(name);
        }

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }
    }
}
