﻿using System;
using System.IO;

namespace Brokeree.DataAccess
{
    /// <summary>
    /// Работа с Csv файлом
    /// Я намеренно не использовал готовую библиотеку,
    /// чтобы максимально ускорить конвертацию данных
    /// </summary>
    public class CsvFile : IDisposable
    {
        /// <summary>
        /// Имя каталога для сохранения файлов
        /// относительно базового каталога приложения
        /// </summary>
        public const string FOLDER = "csv";

        /// <summary>
        /// локальный путь к файлу Csv
        /// </summary>
        public string FullFileName { get; set; }

        /// <summary>
        /// Поток Csv файла
        /// </summary>
        StreamWriter stream;

        /// <summary>
        /// Создание csv файла в специальном каталоге программы
        /// </summary>
        /// <param name="name">имя csv файла</param>
        public CsvFile(string name)
        {
            FullFileName = GetFullNameByCsvFileName(name);
            if (File.Exists(FullFileName))
            {
                throw new AccessViolationException($"Невозможно создать файл {name}, он уже существует");
            }
            stream = new StreamWriter(FullFileName, false);
        }

        /// <summary>
        /// Запись строки в csv файл
        /// </summary>
        /// <param name="str">записываемая строка</param>
        public void Write(string str)
        {
            stream.WriteLine(str);
        }

        /// <summary>
        /// Получить полный путь к каталогу csv файлов
        /// </summary>
        public static string GetBaseFolder()
        {
            return AppDomain.CurrentDomain.BaseDirectory + FOLDER;
        }

        /// <summary>
        /// Возвращает имя csv файла на основе имени бинарного файла и префикса добавляемого к имени
        /// </summary>
        /// <param name="binaryFullFileName">имя бинарного файла</param>
        /// <param name="preffix">добавляемый к имени префикс</param>
        /// <returns>имя csv файла</returns>
        public static string GetName(string binaryFullFileName, string preffix)
        {
            var binFileName = Path.GetFileNameWithoutExtension(binaryFullFileName);
            return $"{binFileName}_{preffix}.csv";
        }

        /// <summary>
        /// Возвращает полный локальный путь к csv файлу
        /// </summary>
        /// <param name="name">имя csv файла</param>
        /// <returns>полный локальный путь к csv файлу</returns>
        public static string GetFullNameByCsvFileName(string name)
        {
            return $"{GetBaseFolder()}\\{name}";
        }

        /// <summary>
        /// Возвращает полный локальный путь к csv файлу на основе имени бинарного файла и префикса добавляемого к имени
        /// </summary>
        /// <param name="binaryFullFileName">имя бинарного файла</param>
        /// <param name="preffix">добавляемый к имени префикс</param>
        /// <returns>полный локальный путь к csv файлу</returns>
        public static string GetFullNameByBinaryFileName(string binaryFullFileName, string preffix)
        {
            var name = GetName(binaryFullFileName, preffix);
            return GetFullNameByCsvFileName(name);
        }

        public void Dispose()
        {
            if (stream != null)
            {
                stream.Dispose();
            }
        }
    }
}
