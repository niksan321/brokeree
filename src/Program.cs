﻿using Brokeree.Model;
using Microsoft.Owin.Hosting;
using NLog;
using System;
using System.IO;
using System.Net;

namespace Brokeree
{
    class Program
    {
        #region константы
#if DEBUG
        public const string DEFAULT_URL = "http://localhost:8080"; //для отладки чтобы не заморачиваться с правами админа
#else
        public const string DEFAULT_URL = "http://*:8080"; //чтобы слушать все Ip адреса нужны права админа
#endif
        #endregion

        /// <summary>
        /// Лог
        /// </summary>
        static Logger log = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
#if DEBUG
            log.Info("Приложение запущено (режим отладки)");
            MakeTestFile();
#else
             log.Info("Приложение запущено");
#endif
            string url = GetUrl(args);
            try
            {
                using (WebApp.Start<Startup>(url))
                {
                    log.Info($"Сервер запущен: {url}");
                    Console.WriteLine("Для выхода нажмите Enter");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                ShowUserFriendlyError(url, ex);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Возвращает адрес привязки www сервера
        /// </summary>
        /// <param name="args">переданные параметры командной строки</param>
        static string GetUrl(string[] args)
        {
            if (args.Length > 0)
            {
                return args[0];
            }
            return DEFAULT_URL;
        }

        /// <summary>
        /// Показ пользователю информации об ошибках в понятной форме
        /// </summary>
        /// <param name="url"></param>
        /// <param name="ex"></param>
        static void ShowUserFriendlyError(string url, Exception ex)
        {
            log.Error($"Невозможно стартовать сервер по адресу: {url}");
            var innerEx = ex.InnerException as HttpListenerException;
            if (innerEx != null)
            {
                switch (innerEx.ErrorCode)
                {
                    case 5:
                        log.Error("Отказано в доступе. Запустите сервер от имени Администратора"); return;
                    case 87:
                    case 1214:
                        log.Error("Адрес не корректен"); return;
                    case 183:
                        log.Error("Порт занят"); return;
                    default:
                        log.Error(innerEx); return;
                }
            }
            else
            {
                log.Error(ex);
            }
        }

        /// <summary>
        /// Создание тестового бинарного файла для отладки
        /// </summary>
        static void MakeTestFile()
        {
            log.Info("Создание тестового файла");
            using (var fs = new FileStream("Тестовый файл.bin", FileMode.Create))
            {
                using (var bw = new BinaryWriter(fs))
                {
                    var header = new BinaryFile.Header
                    {
                        type = "000\"11100001111",
                        version = 1
                    };
                    bw.WriteHeader(ref header);

                    var record = new BinaryFile.TradeRecord();
                    for (int i = 1; i <= 1000; i++)
                    {
                        record.FillTestData(i);
                        bw.Write(ref record);
                    }
                }
            }
            log.Info("Создание тестового файла окончено");
        }
    }
}
