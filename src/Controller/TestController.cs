﻿using Brokeree.Attributes;
using Brokeree.Model;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Web.Http;

namespace Brokeree
{
    /// <summary>
    /// Контроллер для тестирования прозводительность
    /// </summary>
    [RoutePrefix("api/test")]
    [ActionLog]
    [MainExceptionFilter]
    public class TestController : ApiController
    {
        /// <summary>
        /// Тестирование производительсности различных методов чтения/записи структур
        /// (напрямую к задаче данный метод не имеет отношения, 
        /// однако он показывает разницу в скорост обработки данных
        /// и я оставил для наглядности)
        /// </summary>
        /// <param name="repeat">количество повторов тестов</param>
        /// <param name="count">количество чтений/записей в файл</param>
        /// <returns>Результаты тестирования</returns>
        [HttpGet]
        [Route("{repeat=2}/{count=1000000}")]
        public IEnumerable<string> Test(int repeat, int count)
        {
            return new string[]
            {
                TestMarshal(repeat, count),
                TestBinary(repeat, count)
            };
        }

        /// <summary>
        /// Тестирование производительсности чтения/записи структур при прямом чтении/записи
        /// </summary>
        /// <param name="repeat">количество повторов тестов</param>
        /// <param name="count">количество чтений/записей в файл</param>
        /// <returns>Результаты тестирования</returns>
        string TestBinary(int repeat, int count)
        {
            var sw = new Stopwatch();
            sw.Start();
            var record = new BinaryFile.TradeRecord();
            for (int j = 0; j < repeat; j++)
            {
                using (var ms = new MemoryStream())
                {
                    var bw = new BinaryWriter(ms);
                    for (int i = 0; i < count; i++)
                    {
                        record.FillTestData(i);
                        bw.Write(ref record);
                    }
                    ms.Position = 0;
                    var br = new BinaryReader(ms);
                    for (int i = 0; i < count; i++)
                    {
                        br.Read(ref record);
                    }
                    bw.Dispose();
                    br.Dispose();
                }
            }
            sw.Stop();
            return ($"Binary: {sw.Elapsed}");
        }

        /// <summary>
        /// Тестирование производительсности чтения/записи структур при помощи маршалинга
        /// </summary>
        /// <param name="repeat">количество повторов тестов</param>
        /// <param name="count">количество чтений/записей в файл</param>
        /// <returns>Результаты тестирования</returns>
        string TestMarshal(int repeat, int count)
        {
            var sw = new Stopwatch();
            var record = new BinaryFile.TradeRecord();
            sw.Start();
            for (int j = 0; j < repeat; j++)
            {
                using (var ms = new MemoryStream())
                {
                    for (int i = 0; i < count; i++)
                    {
                        record.FillTestData(i);
                        ms.Write(ref record);
                    }
                    ms.Position = 0;
                    for (int i = 0; i < count; i++)
                    {
                        ms.Read(ref record);
                    }
                }
            }
            sw.Stop();
            return ($"Marshal: {sw.Elapsed}");
        }
    }
}
