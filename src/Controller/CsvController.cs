﻿using Brokeree.DataAccess;
using Brokeree.Model;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Brokeree
{
    /// <summary>
    /// Контроллер работы с csv файлом.
    /// Методы не относящиеся к апи обычно выносятся в сервис
    /// и вся бизнес логика также выносится в сервис. 
    /// Сам сервис резолвится через DI контейнер.
    /// Однако в данном случае, для экономии времени, 
    /// а также в силу того что контроллер маленький,
    /// я не считаю необходимым реализовывать модель работы через сервисы KISS
    /// </summary>
    [RoutePrefix("api/csv")]
    public class CsvController : BaseController
    {
        /// <summary>
        /// !!! ВАЖНО передавать полный локальный путь к бинарному файлу 
        /// это огромная дыра в безопасности ТАК ДЕЛАТЬ НЕЛЬЗЯ !!! 
        /// Но коли это условие ТЕСТОВОГО задания я сделал как было написано в условии,
        /// ограничевшись только проверкой расширения файла
        /// Преобразование бинарного файла в csv
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>ссылка на полученнеый csv файл</returns>
        [HttpPost]
        [Route("{*fullFileName}")]
        public string Convert(string fullFileName)
        {
            BinaryFile.Check(fullFileName);
            if (HeaderFileExists(fullFileName) || RecordFileExists(fullFileName))
            {
                return $"Файл {fullFileName} уже обработан или находится в процессе обработки";
            }

            var token = BinaryFile.GetCanseletionTokenSource(fullFileName);
            new Task(() => { ConvertTask(fullFileName, token.Token); }, token.Token).Start();
            return $"Конвертация файла {fullFileName} начата.";
        }

        /// <summary>
        /// Удаление csv файла
        /// </summary>
        /// <param name="fileName">имя файла</param>
        /// <returns>результат удаления</returns>
        [HttpDelete]
        [Route("{*fileName}")]
        public string Delete(string fileName)
        {
            var fullFileName = CsvFile.GetFullNameByCsvFileName(fileName);
            return DeleteStaticFile(fullFileName);
        }

        /// <summary>
        /// Остановка обработки бинарного файла
        /// </summary>
        /// <param name="fullFileName">имя файла</param>
        [HttpPost]
        [Route("stop/{*fullFileName}")]
        public string Stop(string fullFileName)
        {
            BinaryFile.Check(fullFileName);
            return BinaryFile.ConvertCansel(fullFileName);
        }

        /// <summary>
        /// Получить ссылку на сконвертированный файл
        /// </summary>
        /// <param name="fullFileName">имя файла</param>
        [HttpGet]
        [Route("link/{*fullFileName}")]
        public string[] GetLink(string fullFileName)
        {
            BinaryFile.Check(fullFileName);
            if (BinaryFile.IsFileInConvertingProgress(fullFileName))
            {
                return new string[] { $"Файл {fullFileName} ещё обрабаытвается" };
            }

            if (!HeaderFileExists(fullFileName) || !RecordFileExists(fullFileName))
            {
                return new string[] { $"Файл {fullFileName} не обрабатывался" };
            }

            var headerUrl = GetHeaderUrl(fullFileName);
            var recordUrl = GetRecordUrl(fullFileName);

            return new string[] { headerUrl, recordUrl };
        }

        /// <summary>
        /// Возвращает URL к заголовочному файлу csv 
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>URL к заголовочному файлу csv </returns>
        string GetHeaderUrl(string fullFileName)
        {
            var fullName = CsvFile.GetFullNameByBinaryFileName(fullFileName, BinaryFile.Header.TABLE_NAME);
            return MapPathReverse(fullName);
        }

        /// <summary>
        /// Возвращает URL к файлу записей csv 
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>URL к заголовочному файлу csv </returns>
        string GetRecordUrl(string fullFileName)
        {
            var fullName = CsvFile.GetFullNameByBinaryFileName(fullFileName, BinaryFile.TradeRecord.TABLE_NAME);
            return MapPathReverse(fullName);
        }

        /// <summary>
        /// Задача по конвертированию бинарного файла,
        /// выполняемая в отдельном потоке
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <param name="token">токен отмены задачи</param>
        void ConvertTask(string fullFileName, CancellationToken token)
        {
            try
            {
                using (var fs = new FileStream(fullFileName, FileMode.Open))
                {
                    using (var reader = new BinaryReader(fs))
                    {
                        ConvertHeader(reader, fullFileName);
                        ConvertRecords(reader, fullFileName, token);
                    }
                }
            }
            catch (OperationCanceledException)
            {
                DeleteCsvFiles(fullFileName);
            }
            catch (Exception ex)
            {
                DeleteCsvFiles(fullFileName);
                log.Error(ex);
            }
            finally
            {
                BinaryFile.ConvertEnd(fullFileName);
            }
        }

        /// <summary>
        /// Преобразование заголовка бинарного файла в csv
        /// </summary>
        /// <param name="reader">поток бинарного файла</param>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        void ConvertHeader(BinaryReader reader, string fullFileName)
        {
            var headerName = CsvFile.GetName(fullFileName, BinaryFile.Header.TABLE_NAME);
            using (var csv = new CsvFile(headerName))
            {
                var header = reader.ReadHeader();
                csv.Write(header.ToCsv());
            }
        }

        /// <summary>
        /// Преобразование записей бинарного файла в csv
        /// </summary>
        /// <param name="reader">поток бинарного файла</param>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <param name="token">токен отмены задачи</param>
        void ConvertRecords(BinaryReader reader, string fullFileName, CancellationToken token)
        {
            var recordName = CsvFile.GetName(fullFileName, BinaryFile.TradeRecord.TABLE_NAME);
            using (var csv = new CsvFile(recordName))
            {
                var recordSize = Marshal.SizeOf(typeof(BinaryFile.TradeRecord));
                var streamLength = reader.BaseStream.Length;
                var record = new BinaryFile.TradeRecord();
                while (streamLength - reader.BaseStream.Position >= recordSize)
                {
                    //Thread.Sleep(100);
                    token.ThrowIfCancellationRequested();
                    reader.Read(ref record);
                    csv.Write(record.ToCsv());
                }
            }
        }

        /// <summary>
        /// Проверяет существует ли csv файл заголовка
        /// </summary>
        /// <param name="fullFileName">полны локальный путь к бинарному файлу</param>
        /// <returns>true - существует, false - нет</returns>
        bool HeaderFileExists(string fullFileName)
        {
            var fullName = CsvFile.GetFullNameByBinaryFileName(fullFileName, BinaryFile.Header.TABLE_NAME);
            if (File.Exists(fullName))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Проверяет существует ли csv файл записей
        /// </summary>
        /// <param name="fullFileName">полны локальный путь к бинарному файлу</param>
        /// <returns>true - существует, false - нет</returns>
        bool RecordFileExists(string fullFileName)
        {
            var fullName = CsvFile.GetFullNameByBinaryFileName(fullFileName, BinaryFile.TradeRecord.TABLE_NAME);
            if (File.Exists(fullName))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Удаляет заголовочный csv файл и csv файл записей
        /// соответсвующие бинарному файлу
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        void DeleteCsvFiles(string fullFileName)
        {
            try
            {
                var recordFullName = CsvFile.GetFullNameByBinaryFileName(fullFileName, BinaryFile.TradeRecord.TABLE_NAME);
                if (File.Exists(recordFullName))
                {
                    File.Delete(recordFullName);
                }

                var headerFullName = CsvFile.GetFullNameByBinaryFileName(fullFileName, BinaryFile.Header.TABLE_NAME);
                if (File.Exists(headerFullName))
                {
                    File.Delete(headerFullName);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
