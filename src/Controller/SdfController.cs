﻿using Brokeree.DataAccess;
using Brokeree.Model;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using static Brokeree.Model.BinaryFile;

namespace Brokeree
{
    /// <summary>
    /// Контроллер работы с sdf файлом базы данных.
    /// Методы не относящиеся к апи обычно выносятся в сервис
    /// и вся бизнес логика также выносится в сервис. 
    /// Сам сервис резолвится через DI контейнер.
    /// Однако в данном случае, для экономии времени, 
    /// а также в силу того что контроллер маленький,
    /// я не считаю необходимым реализовывать модель работы через сервисы KISS
    /// </summary>
    [RoutePrefix("api/sdf")]
    public class SdfController : BaseController
    {
        /// <summary>
        /// !!! ВАЖНО передавать полный локальный путь к бинарному файлу 
        /// это огромная дыра в безопасности ТАК ДЕЛАТЬ НЕЛЬЗЯ !!! 
        /// Но коли это условие ТЕСТОВОГО задания я сделал как было написано в условии,
        /// ограничевшись только проверкой расширения файла
        /// Преобразование бинарного файла в файл sdf базы данных
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>ссылка на sdf файл базы данных</returns>
        [HttpPost]
        [Route("{*fullFileName}")]
        public string Convert(string fullFileName)
        {
            BinaryFile.Check(fullFileName);
            if (SdfFileExists(fullFileName))
            {
                return $"Файл {fullFileName} уже обработан или находится в процессе обработки";
            }

            var token = BinaryFile.GetCanseletionTokenSource(fullFileName);
            new Task(() => { ConvertTask(fullFileName, token.Token); }, token.Token).Start();
            return $"Конвертация файла {fullFileName} начата.";
        }

        /// <summary>
        /// Получить запись по её Id
        /// </summary>
        /// <param name="fileName">имя файла базы данных в формате sdf</param>
        /// <param name="id">Id требуемой записи</param>
        /// <returns>запись</returns>
        [HttpGet]
        [Route("{id}/{*fileName}")]
        public TradeRecord GetRecord(string fileName, int id)
        {
            var query = TradeRecord.SqlGetRecordCommand(id);
            using (var sdf = new SdfFile(fileName, false))
            {
                using (var command = sdf.GetCommand(query))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var record = new TradeRecord();
                            reader.Read(ref record);
                            return record;
                        }
                        else
                        {
                            throw new Exception($"В файле {fileName} не найдена запись с id={id}");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// удаление sdf файла базы данных
        /// </summary>
        /// <param name="fileName">имя sdf файла базы данных</param>
        /// <returns>результат удаления</returns>
        [HttpDelete]
        [Route("{*fileName}")]
        public string Delete(string fileName)
        {
            var fullFileName = SdfFile.GetFullNameBySdfFileName(fileName);
            return DeleteStaticFile(fullFileName);
        }

        /// <summary>
        /// Остановка обработки бинарного файла
        /// </summary>
        /// <param name="fullFileName">имя файла</param>
        [HttpPost]
        [Route("stop/{*fullFileName}")]
        public string Stop(string fullFileName)
        {
            BinaryFile.Check(fullFileName);
            return BinaryFile.ConvertCansel(fullFileName);
        }

        /// <summary>
        /// Получить ссылку на сконвертированный файл
        /// </summary>
        /// <param name="fullFileName">имя файла</param>
        [HttpGet]
        [Route("link/{*fullFileName}")]
        public string GetLink(string fullFileName)
        {
            BinaryFile.Check(fullFileName);
            if (BinaryFile.IsFileInConvertingProgress(fullFileName))
            {
                return $"Файл {fullFileName} ещё обрабаытвается";
            }

            if (!SdfFileExists(fullFileName))
            {
                return $"Файл {fullFileName} не обрабатывался";
            }

            var sdfFullName = SdfFile.GetFullNameByBinaryFileName(fullFileName);
            return MapPathReverse(sdfFullName);
        }

        /// <summary>
        /// Задача по конвертированию бинарного файла,
        /// выполняемая в отдельном потоке
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <param name="token">токен отмены задачи</param>
        void ConvertTask(string fullFileName, CancellationToken token)
        {
            try
            {
                using (var fs = new FileStream(fullFileName, FileMode.Open))
                {
                    using (var reader = new BinaryReader(fs))
                    {
                        var sdfName = SdfFile.GetName(fullFileName);
                        using (var sdf = new SdfFile(sdfName, true))
                        {
                            ConvertBinaryFileToSdf(reader, sdf, token);
                        }
                    }
                }
            }
            catch (OperationCanceledException)
            {
                DeleteSdfFile(fullFileName);
            }
            catch (Exception ex)
            {
                DeleteSdfFile(fullFileName);
                log.Error(ex);
            }
            finally
            {
                BinaryFile.ConvertEnd(fullFileName);
            }
        }

        /// <summary>
        /// Удаление sdf файла по полному локальному пути к бинарному файлу
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        void DeleteSdfFile(string fullFileName)
        {
            try
            {
                var sdfFullName = SdfFile.GetFullNameByBinaryFileName(fullFileName);
                if (File.Exists(sdfFullName))
                {
                    File.Delete(sdfFullName);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        /// <summary>
        /// Преобразование бинарного файла в формат sdf базы данных
        /// </summary>
        /// <param name="reader">поток бинарного файла</param>
        /// <param name="sdf">sdf файл базы данных</param>
        /// <param name="token">токен отмены</param>
        void ConvertBinaryFileToSdf(BinaryReader reader, SdfFile sdf, CancellationToken token)
        {
            ConvertHeader(reader, sdf);
            ConvertRecords(reader, sdf, token);
        }

        /// <summary>
        /// Сохранение записей бинарного файла в бд 
        /// </summary>
        /// <param name="reader">поток бинарного файла</param>
        /// <param name="sdf">sdf файл бд</param>
        /// <param name="token">токен отмены</param>
        void ConvertRecords(BinaryReader reader, SdfFile sdf, CancellationToken token)
        {
            sdf.ExecuteSqlNoneQuery(TradeRecord.SqlCreateTableCmd());
            using (var command = sdf.GetDirectCommand(TradeRecord.TABLE_NAME))
            {
                using (var set = sdf.GetSet(command))
                {
                    var dbRecord = set.CreateRecord();
                    var recordSize = Marshal.SizeOf(typeof(TradeRecord));
                    var streamLength = reader.BaseStream.Length;
                    var record = new TradeRecord();
                    while (streamLength - reader.BaseStream.Position >= recordSize)
                    {
                        //Thread.Sleep(100);
                        token.ThrowIfCancellationRequested();
                        reader.Read(ref record);
                        dbRecord.Write(ref record);
                        set.Insert(dbRecord);
                    }
                }
            }
        }

        /// <summary>
        /// Сохранение заголовка бинарного файла в sdf файл бд
        /// </summary>
        /// <param name="reader">поток бинарного файла</param>
        /// <param name="sdf">sdf файл бд</param>
        void ConvertHeader(BinaryReader reader, SdfFile sdf)
        {
            sdf.ExecuteSqlNoneQuery(Header.GetSqlCreateTableCmd());
            var header = reader.ReadHeader();
            sdf.WriteHeader(ref header);
        }

        /// <summary>
        /// Проверяет существует ли sdf файл бд
        /// </summary>
        /// <param name="fullFileName">полны локальный путь к бинарному файлу</param>
        /// <returns>true - существует, false - нет</returns>
        bool SdfFileExists(string fullFileName)
        {
            var sdfFullName = SdfFile.GetFullNameByBinaryFileName(fullFileName);
            if (File.Exists(sdfFullName))
            {
                return true;
            }
            return false;
        }
    }
}
