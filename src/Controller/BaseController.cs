﻿using Brokeree.Attributes;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;

namespace Brokeree
{
    /// <summary>
    /// Базовый контроллер
    /// </summary>
    [ActionLog]
    [MainExceptionFilter]
    public abstract class BaseController : ApiController
    {
        /// <summary>
        /// Лог
        /// </summary>
        protected static Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Преобразование локального пути к файлу в url адрес
        /// </summary>
        /// <param name="localPath">полный локальный путь к файлу</param>
        /// <returns>полный url путь к файлу</returns>
        public string MapPathReverse(string localPath)
        {
            string host;
            var uri = Request.RequestUri;
            if (uri.Port == 80)
            {
                host = $"{uri.Scheme}://{uri.Host}";
            }
            else
            {
                host = $"{uri.Scheme}://{uri.Host}:{uri.Port}";
            }
            var path = localPath.Replace(AppDomain.CurrentDomain.BaseDirectory, "")
                                      .Replace(@"\", "/");
            return $"{host}/{path}";
        }

        /// <summary>
        /// Удаление файла
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к файлу</param>
        /// <returns>результат удаления</returns>
        public string DeleteStaticFile(string fullFileName)
        {
            var fileName = Path.GetFileName(fullFileName);
            if (File.Exists(fullFileName))
            {
                try
                {
                    File.Delete(fullFileName);
                    return $"Файл {fileName} удалён";
                }
                catch
                {
                    return $"Файл {fileName} заблокирован";
                }
            }
            return $"Файл {fileName} не найден";
        }
    }
}
