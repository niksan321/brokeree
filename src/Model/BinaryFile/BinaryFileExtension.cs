﻿using Brokeree.DataAccess;
using System.Data.SqlServerCe;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using static Brokeree.Model.BinaryFile;

namespace Brokeree.Model
{
    /// <summary>
    /// Расширения для чтения/записи структур BinaryFile
    /// Согласно исследованию:
    /// http://genericgamedev.com/general/converting-between-structs-and-byte-arrays/
    /// самый быстрый метод использовать BinaryReader/Writer
    /// однако если структура разрастётся или будут частые изменения в структуре,
    /// и если скорость чтения/записи не сильно критична
    /// то лучше использовать маршалинг (поддержка будет проще)
    /// (все методы расширения заточены под максимальную производительность,
    /// в том числе поэтому используется строгий порядок полей и передача структур по ссылке, 
    /// передача структур по ссылке при чтении/записи также позволяет сократить время на выделение памяти,
    /// так как структуру можно создать один раз и переиспользовать при каждом чтении/записи)
    /// </summary>
    public static class BinaryFileExtension
    {
        /// <summary>
        /// Чтение заголовка из потока
        /// </summary>
        /// <param name="reader">поток</param>
        /// <returns>считанный заголовок</returns>
        public static Header ReadHeader(this BinaryReader reader)
        {
            return new Header
            {
                version = reader.ReadInt32(),
                type = new string(reader.ReadChars(Header.TYPE_SIZE)).Trim()
            };
        }

        /// <summary>
        /// запис заголовка в поток
        /// </summary>
        /// <param name="writer">поток</param>
        /// <param name="header">заголовок</param>
        public static void WriteHeader(this BinaryWriter writer, ref Header header)
        {
            writer.Write(header.version);
            var arr = header.type.PadRight(Header.TYPE_SIZE).ToCharArray();
            writer.Write(arr, 0, Header.TYPE_SIZE);
        }

        /// <summary>
        /// Так как предполагается частое чтение записей из файла,
        /// то лучше использовать одну структуру и передавать её по ссылке, 
        /// хотя структура внутри метода и не изменяется
        /// </summary>
        /// <param name="reader">поток для чтения данных</param>
        /// <param name="record">считанная запись</param>
        public static void Read(this BinaryReader reader, ref TradeRecord record)
        {
            record.id = reader.ReadInt32();
            record.account = reader.ReadInt32();
            record.volume = reader.ReadDouble();
            record.comment = new string(reader.ReadChars(TradeRecord.COMMENT_SIZE)).Trim();
        }

        /// <summary>
        /// Так как предполагается частая запись в файл,
        /// то лучше использовать одну структуру и передавать её по ссылке,
        /// хотя структура внутри метода и не изменяется
        /// </summary>
        /// <param name="writer">поток для записи</param>
        /// <param name="record">записываемый объект</param>
        public static void Write(this BinaryWriter writer, ref TradeRecord record)
        {
            writer.Write(record.id);
            writer.Write(record.account);
            writer.Write(record.volume);
            var arr = record.comment.PadRight(TradeRecord.COMMENT_SIZE).ToCharArray();
            writer.Write(arr, 0, TradeRecord.COMMENT_SIZE);
        }

        /// <summary>
        /// Generic метод чтения записи из потока,
        /// можно использовать если скорость чтения не сильно критична,
        /// а лёгкость поддержки кода стоит на первом месте
        /// </summary>
        /// <typeparam name="T">тип считываемой сущности</typeparam>
        /// <param name="stream">поток для чтения</param>
        /// <param name="obj">считанная сущность</param>
        public static void Read<T>(this Stream stream, ref T obj)
        {
            byte[] buffer = new byte[Marshal.SizeOf(typeof(T))];
            stream.Read(buffer, 0, Marshal.SizeOf(typeof(T)));
            GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            obj = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();
        }

        /// <summary>
        /// Generic метод для записи сущности в поток
        /// можно использовать если скорость записи не сильно критична,
        /// а лёгкость поддержки кода стоит на первом месте
        /// </summary>
        /// <typeparam name="T">тип записываемой сущности</typeparam>
        /// <param name="stream">поток</param>
        /// <param name="obj">записываемая сущность</param>
        public static void Write<T>(this Stream stream, ref T obj)
        {
            byte[] buffer = new byte[Marshal.SizeOf(typeof(T))];
            GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            Marshal.StructureToPtr(obj, handle.AddrOfPinnedObject(), false);
            stream.Write(buffer, 0, Marshal.SizeOf(typeof(T)));
            handle.Free();
        }

        /// <summary>
        /// Записать TradeRecord в SqlCeUpdatableRecord
        /// </summary>
        public static void Write(this SqlCeUpdatableRecord dbRecord, ref TradeRecord tRecord)
        {
            dbRecord.SetInt32(0, tRecord.id);
            dbRecord.SetInt32(1, tRecord.account);
            dbRecord.SetDouble(2, tRecord.volume);
            dbRecord.SetString(3, tRecord.comment);
        }

        /// <summary>
        /// Запись заголовка в бд
        /// </summary>
        public static void WriteHeader(this SdfFile sdf, ref Header header)
        {
            using (var command = sdf.GetDirectCommand(Header.TABLE_NAME))
            {
                using (var set = sdf.GetSet(command))
                {
                    var dbRecord = set.CreateRecord();
                    dbRecord.SetInt32(0, header.version);
                    dbRecord.SetString(1, header.type);
                    set.Insert(dbRecord);
                }
            }
        }

        /// <summary>
        /// Чтение записи
        /// </summary>
        public static void Read(this SqlCeDataReader reader, ref TradeRecord record)
        {
            record.id = reader.GetInt32(0);
            record.account = reader.GetInt32(1);
            record.volume = reader.GetDouble(2);
            record.comment = reader.GetString(3);
        }
    }
}
