﻿namespace Brokeree.Model
{
    /// <summary>
    /// Описывает структуру бинарного файла
    /// </summary>
    public abstract partial class BinaryFile
    {
        /// <summary>
        /// Запись
        /// </summary>
        public partial struct TradeRecord
        {
            /// <summary>
            /// возвращает sql запрос на создание таблицы данной структуры
            /// я не уверен что для таблицы требуется контроль уникальности поля id
            /// в силу данных причин я не стал вешать основной ключ на это поле
            /// так как это хоть и не сильно, но замедлило бы вставку значений
            /// </summary>
            public static string SqlCreateTableCmd()
            {
                string query = $@"create table {TABLE_NAME}
                                 (
                                    id int, 
                                    account int, 
                                    volume float, 
                                    comment nvarchar({COMMENT_SIZE})
                                 )";
                return query;
            }

            /// <summary>
            /// Запрос на извлечение одной записи.
            /// Здесь важна последовательность возвращаемых полей, 
            /// она должна совпадать с самой последовательностью полей структуры
            /// </summary>
            /// <param name="id">Ид требуемой</param>
            /// <returns>sql запрос выборки записи из бд</returns>
            public static string SqlGetRecordCommand(int id)
            {
                string query = $"SELECT [id], [account], [volume], [comment] FROM {TABLE_NAME} WHERE id={id}";
                return query;
            }

            /// <summary>
            /// можно было бы сделать чтение полей и их значений через рефлексию,
            /// но при потоковой записи в csv файл так будет быстрее, 
            /// плюс опять таки, через рефлексию нельзя контролировать последовательность доступа к полям
            /// (если конечно их не сортировать), но я подразумеваю строго заданную последовательность полей
            /// </summary>
            /// <returns>строковое представление записи для csv файла</returns>
            public string ToCsv()
            {
                return $"{id},{account},{DoubleToCsvString(volume)},\"{StringToCsvString(comment)}\"";
            }

            /// <summary>
            /// Заполняет поля тестовыми данными на основе Id
            /// </summary>
            /// <param name="recordId">id на основе которого строятся тестовые данные</param>
            public void FillTestData(int recordId)
            {
                id = recordId;
                account = 10000 + id;
                volume = (double)id / 1000;
                comment = $"Коммент \" {id}";
            }
        }
    }
}
