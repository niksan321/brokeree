﻿using System.Runtime.InteropServices;

namespace Brokeree.Model
{
    /// <summary>
    /// Описывает структуру бинарного файла
    /// </summary>
    public abstract partial class BinaryFile
    {
        /// <summary>
        /// Запись
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public partial struct TradeRecord
        {
            #region константы
            /// <summary>
            /// Имя таблицы при конвертировании в бд структуры TradeRecord
            /// </summary>
            public const string TABLE_NAME = "trade_record";

            /// <summary>
            /// Размер поля comment TradeRecord
            /// </summary>
            public const int COMMENT_SIZE = 64; 
            #endregion

            public int id;
            public int account;
            public double volume;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = COMMENT_SIZE)]
            public string comment;
        }
    }
}
