﻿namespace Brokeree.Model
{
    /// <summary>
    /// Описывает структуру бинарного файла
    /// </summary>
    public abstract partial class BinaryFile
    {
        /// <summary>
        /// Заголовок
        /// </summary>
        public partial struct Header
        {
            /// <summary>
            /// можно было бы сделать чтение полей и их значений через рефлексию,
            /// но так как полей у заголовка мало и заголовок формирует часть имени файла,
            /// а используя рефлексию нельзя контролировать последовательность полей
            /// то лучше просто формировать строку с требуемым порядком полей
            /// </summary>
            /// <returns>информационная строка о заголовке</returns>
            public string ToCsv()
            {
                return  $"{version},\"{StringToCsvString(type)}\"";
            }

            /// <summary>
            /// возвращает sql запрос на создание таблицы даннйо структуры 
            /// </summary>
            public static string GetSqlCreateTableCmd()
            {
                string query = $@"create table {TABLE_NAME}
                                 (
                                    version int, 
                                    type nvarchar({TYPE_SIZE})
                                 )";
                return query;
            } 
        }
    }
}
