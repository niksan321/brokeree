﻿using System.Runtime.InteropServices;

namespace Brokeree.Model
{
    /// <summary>
    /// Описывает структуру бинарного файла
    /// </summary>
    public abstract partial class BinaryFile
    {
        /// <summary>
        /// Заголовок
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public partial struct Header
        {
            #region константы
            /// <summary>
            /// Размер поля type Header
            /// </summary>
            public const int TYPE_SIZE = 16;

            /// <summary>
            /// Имя таблицы при конвертировании в бд структуры Header
            /// </summary>
            public const string TABLE_NAME = "header"; 
            #endregion

            public int version;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = TYPE_SIZE)]
            public string type;
        }
    }
}
