﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace Brokeree.Model
{
    /// <summary>
    /// Описывает структуру бинарного файла
    /// </summary>
    public abstract partial class BinaryFile
    {
        /// <summary>
        /// Расширение бинарного файла
        /// </summary>
        public const string FILE_EXTENSION = ".bin";

        /// <summary>
        /// Список бинарных файлов, которые находятся в процессе обработки
        /// и соответсвующие токены для отмены обработки
        /// </summary>
        public static Dictionary<string, CancellationTokenSource> BinaryFileInProgress = new Dictionary<string, CancellationTokenSource>();

        /// <summary>
        /// Переменная для хранения сущности для итератора
        /// ThreadStatic - нужен для потокобезопасности
        /// </summary>
        [ThreadStatic]
        static TradeRecord record;

        /// <summary>
        /// Итератор для чтения данных из потока, 
        /// служит для более простого чтения данных из бинарного файла,
        /// однако, если требуется выжать максимум скорости чтения, то лучше отказатся от итератора.
        /// (собственно в проекте я не стал его использовать, но как пример кода может быть полезен,
        ///  можно использовать в цикле foreach для перебора всех записей в бинарном файле)
        /// </summary>
        /// <param name="reader">поток для чтения</param>
        /// <returns>считанную запись</returns>
        public static IEnumerable<TradeRecord> GetRecords(BinaryReader reader)
        {
            var recordSize = Marshal.SizeOf(typeof(TradeRecord));
            var length = reader.BaseStream.Length;

            while (length - reader.BaseStream.Position >= recordSize)
            {
                reader.Read(ref record);
                yield return record;
            }
        }

        /// <summary>
        /// Проверка корректен ли бинарный файл по указанному пути
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        public static void Check(string fullFileName)
        {
            if (!Path.IsPathRooted(fullFileName))
            {
                throw new Exception($"Путь {fullFileName} должен быть абсолютным");
            }

            if (!File.Exists(fullFileName))
            {
                throw new Exception($"Файл {fullFileName} не найден");
            }

            if (Path.GetExtension(fullFileName).ToLower() != FILE_EXTENSION.ToLower())
            {
                throw new Exception($"Расширение файла должно быть {FILE_EXTENSION}");
            }
        }

        /// <summary>
        /// Отмена конвертации бинарного файла
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>информация о прерваном конвертировании</returns>
        public static string ConvertCansel(string fullFileName)
        {
            lock (BinaryFileInProgress)
            {
                if (BinaryFileInProgress.ContainsKey(fullFileName))
                {
                    var tokenSource = BinaryFileInProgress[fullFileName];
                    tokenSource.Cancel();
                    BinaryFileInProgress.Remove(fullFileName);
                    return $"Обработка файла {fullFileName} прервана";
                }
                else
                {
                    return $"Файл {fullFileName} не обрабатывается";
                }
            }
        }

        /// <summary>
        /// Возвращает токен отмены обработки бинарного файла
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>токен отмены</returns>
        public static CancellationTokenSource GetCanseletionTokenSource(string fullFileName)
        {
            lock (BinaryFileInProgress)
            {
                if (BinaryFileInProgress.ContainsKey(fullFileName))
                {
                    throw new Exception($"Файл {fullFileName} уже обрабатывается, дождитесь окончания");
                }
                else
                {
                    var tokenSource = new CancellationTokenSource();
                    BinaryFileInProgress.Add(fullFileName, tokenSource);
                    return tokenSource;
                }
            }
        }

        /// <summary>
        /// Бинарнй файл в процессе конвертации?
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу</param>
        /// <returns>true - файл в обработке, false - нет</returns>
        public static bool IsFileInConvertingProgress(string fullFileName)
        {
            lock (BinaryFileInProgress)
            {
                if (BinaryFileInProgress.ContainsKey(fullFileName))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// удаляет бинарный файл из списка конвертируемых в текущий момент файлов (окончание конвертации)
        /// </summary>
        /// <param name="fullFileName">полный локальный путь к бинарному файлу, который конвертировался</param>
        public static void ConvertEnd(string fullFileName)
        {
            lock (BinaryFileInProgress)
            {
                if (BinaryFileInProgress.ContainsKey(fullFileName))
                {
                    BinaryFileInProgress.Remove(fullFileName);
                }
            }
        }

        /// <summary>
        /// Возвращает строку исправленную под формат csv файла
        /// можно было бы данный метод поместить в класс CsvFile
        /// но тогда данный файл зависил бы от класса CsvFile
        /// только данным методом, а пока других связей нет лучше оставить это здесь
        /// </summary>
        /// <param name="str">преобразуемая строка</param>
        /// <returns>строка исправленная под формат csv файла</returns>
        public static string StringToCsvString(string str)
        {
            if (str.IndexOfAny(new char[] { '"', ',' }) != -1)
            {
                return $"\"{str.Replace("\"", "\"\"")}\"";
            }
            return str;
        }

        /// <summary>
        /// преобразует double в строку с разделителем - точка
        /// </summary>
        /// <param name="d">преобразуемое число</param>
        /// <returns>строка исправленная под формат csv файла</returns>
        public static string DoubleToCsvString(double d)
        {
            var cultureInfo = System.Globalization.CultureInfo.InvariantCulture;
            return $"{d.ToString(cultureInfo)}";
        }
    }
}
